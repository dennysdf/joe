﻿using JOE.Models.DBJOE.Context;
using JOE.Models.DBJOE.Tables;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace JOE.Areas.Orientador.Models.Relatorios
{
    public class RelatorioVM
    {
        public int Id { get; set; }
        [Required(ErrorMessage = Global.Required)]
        public string Descricao { get; set; }
        public IFormFile File { get; set; }
        public SelectList Orientandos { get; set; }
        public int EstagioId { get; set; }
        public string Data { get; internal set; }
        public bool IsFile { get; internal set; }
        public string NameFile { get; internal set; }
        public int ResponsavelId { get; internal set; }
        public string Align { get; internal set; }
        public string Orientador { get; internal set; }
        public string Estagiario { get; internal set; }
        public int IdSelected { get; set; }
        public string Responsavel { get; internal set; }

        public RelatorioVM()
        {

        }

        public RelatorioVM(DBJOEContext _context, int id, int idAluno)
        {
            var alunos = _context.OrientadoresEstagiarios
                        .Include(c => c.Estagiario)
                        .Where(c => c.OrientadorId == id && c.IsAtivo && !c.IsFinalizado)
                        .Select(c => new { c.Id, c.Estagiario.Nome })
                        .ToList();
            EstagioId = idAluno;
            IdSelected = idAluno;
            Orientandos = new SelectList(alunos, "Id", "Nome");
        }

        public Relatorio Insert(int id)
        {
            return new Relatorio
            {
                Descricao = this.Descricao,
                EstagioId = this.EstagioId,
                NameFile = File != null ? Path.GetFileName(File.FileName) : null,
                ResponsavelId = id
            };
        }
    }
}
