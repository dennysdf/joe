﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JOE.Areas.Orientador.Models.Estagiarios
{
    public class EstagiariosVM
    {

        public string Nome { get; set; }
        public string Empresa { get; set; }
        public string Fase { get; set; }
        public int AlunoId { get; set; }
        public int EmpresaId { get; set; }
        public string Previsao { get; internal set; }
    }
}