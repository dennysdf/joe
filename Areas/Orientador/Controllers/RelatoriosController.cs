﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using JOE.Areas.Orientador.Models.Relatorios;
using JOE.Models.DBJOE.Context;
using JOE.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace JOE.Areas.Orientador.Controllers
{
    [Area("Orientador")]
    [Authorize(Policy = "Orientador")]
    public class RelatoriosController : Controller
    {
        private readonly DBJOEContext _context;
        private readonly IUser _user;
        public readonly INotificacao _notify;

        public RelatoriosController(DBJOEContext context, IUser user, INotificacao notify)
        {
            _context = context;
            _user = user;
            _notify = notify;
        }

        public IActionResult Index(int id = 0) => View(new RelatorioVM(_context, _user.Id, id));

        [HttpPost]
        public IActionResult Index(RelatorioVM model)
        {
            var relatorio = model.Insert(_user.Id);
            _context.Add(relatorio);
            _context.SaveChanges();

            var diretorio = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\Upload\\Relatorios\\"+ model.EstagioId +"");

            if (model.File != null)
            {
                if (!Directory.Exists(diretorio)) Directory.CreateDirectory(diretorio);

                var nFiles = Directory.GetFiles(diretorio);
                
                using (var stream = new FileStream(Path.Combine(diretorio, relatorio.Id.ToString() +"_"+  Path.GetFileName(model.File.FileName) ), FileMode.Create))
                {
                    model.File.CopyTo(stream);
                }                
            }
            _notify.Success("Mensagem enviada com sucesso.");
            return RedirectToAction(nameof(Index), new { id = model.EstagioId});
        }

        public IActionResult AjaxRelatorios(int id)
        {
            var relatorio = _context.Relatorios
                .Include(c => c.Responsavel)
                .Include(c => c.Estagio)
                    .ThenInclude(c => c.Orientador)
                .Include(c => c.Estagio)
                    .ThenInclude(c => c.Estagiario)
                .Where(c => c.EstagioId == id)
                .Select(c => new RelatorioVM
                {
                    EstagioId = c.EstagioId,
                    Descricao=  c.Descricao,
                    Data = c.DateAdd.ToString("dd MMMM yyyy"),
                    IsFile = !string.IsNullOrEmpty(c.NameFile),
                    NameFile = $"{c.Id}_{c.NameFile}" ,
                    Responsavel = c.Responsavel.Nome,
                    Align = c.ResponsavelId == _user.Id ? "self" : "other",
                    Orientador = c.Estagio.Orientador.Nome,
                    Estagiario = c.Estagio.Estagiario.Nome,
                })
                .ToList();

            return View(relatorio);
        }

    }
}