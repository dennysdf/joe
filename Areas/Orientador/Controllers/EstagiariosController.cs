﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JOE.Areas.Admin.Models.Alunos;
using JOE.Areas.Admin.Models.Empresas;
using JOE.Areas.Orientador.Models.Estagiarios;
using JOE.Models.DBJOE.Context;
using JOE.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace JOE.Areas.Orientador.Controllers
{
    [Area("Orientador")]
    [Authorize(Policy = "Orientador")]
    public class EstagiariosController : Controller
    {
        private readonly DBJOEContext _context;
        private readonly IUser _user;
        public readonly INotificacao _notify;

        public EstagiariosController(DBJOEContext context, IUser user, INotificacao notify)
        {
            _context = context;
            _user = user;
            _notify = notify;
        }

        public IActionResult Index()
        {
            var estagios = _context.OrientadoresEstagiarios
                .Include(c => c.Estagiario)
                .Include(c => c.Empresa)
                .Where(c => c.OrientadorId == _user.Id && c.Estagiario.IsAtivo && c.Empresa.IsAtivo && c.IsAtivo && !c.IsFinalizado)
                .Select(c => new EstagiariosVM
                {
                    Nome = c.Estagiario.Nome,
                    Empresa = c.Empresa.Nome,
                    Fase = c.Previsao >= DateTime.Now ? "Realizando estágio." : c.Previsao.AddDays(90) >= DateTime.Now ? "Relatório de estágio em produção." : "<span class=text-danger>Prazo de relatório de estágio estourado.</span>",
                    AlunoId = c.EstagiarioId,
                    EmpresaId = c.EmpresaId,
                    Previsao = c.Previsao >= DateTime.Now ? "<b>Fim do estágio:</b> " + c.Previsao.ToShortDateString() + " " : c.Previsao.AddDays(90) >= DateTime.Now ? "<b>Fim do período de relatório:</b> " + c.Previsao.AddDays(90).ToShortDateString() + " " : "<b>Fim do período de relatório:</b> " + c.Previsao.AddDays(90).ToShortDateString() + " "
                })
                .ToList();

            return View(estagios);
        }

        public IActionResult AjaxEstagiario(int id)
        {
            return View(new AlunoVM(_context, id));
        }

        public IActionResult AjaxEmpresa(int id)
        {
            return View(new EmpresaVM(_context, id));
        }
    }
}