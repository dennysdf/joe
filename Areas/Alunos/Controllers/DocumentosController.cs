﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JOE.Areas.Alunos.Controllers
{
    [Area("Alunos")]
    [Authorize(Policy = "Alunos")]
    public class DocumentosController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}