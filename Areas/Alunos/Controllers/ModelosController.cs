﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JOE.Models.DBJOE.Context;
using JOE.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JOE.Areas.Alunos.Controllers
{
    [Area("Alunos")]
    [Authorize(Policy = "Alunos")]
    public class ModelosController : Controller
    {
        private readonly IUser _user;
        private readonly DBJOEContext _context;

        public ModelosController(IUser user, DBJOEContext context)
        {
            _user = user;
            _context = context;
        }

        public IActionResult Index()
        {
            // FAZER FUNÇÃO AAQUI

            return View();
        }
    }
}