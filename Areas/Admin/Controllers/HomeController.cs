﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JOE.Models.DBJOE.Context;
using JOE.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JOE.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Policy = "Admin")]
    public class HomeController : Controller
    {
        private readonly DBJOEContext _context;
        private readonly IUser _user;

        public HomeController(DBJOEContext context, IUser user)
        {
            _context = context;
            _user = user;
        }

        public IActionResult Index()
        {
            var a = _user.Id;

            return View();
        }
    }
}