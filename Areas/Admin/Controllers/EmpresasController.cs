﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JOE.Areas.Admin.Models.Empresas;
using JOE.Models.DBJOE.Context;
using JOE.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JOE.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Policy = "Admin")]
    public class EmpresasController : Controller
    {
        private readonly DBJOEContext _context;
        private readonly IUser _user;
        private readonly INotificacao _notify;

        public EmpresasController(DBJOEContext context, IUser user, INotificacao notidy)
        {
            _context = context;
            _user = user;
            _notify = notidy;
        }

        public IActionResult Index()
        {
            var empresa = _context.Empresas.Where(c => c.IsAtivo).Select(c => new EmpresaVM
            {
                Id = c.Id,
                Nome = c.Nome,
                CNPJ = c.CNPJ,
                AreaAtuacao = c.AreaAtuacao,
                Endereco = c.Endereco,
                RazaoSocial = c.RazaoSocial,
                Servico = c.Servico,
                Site = c.Site,
                Telefone = c.Telefone
            }).ToList();

            return View(empresa);
        }

        [HttpPost]
        public IActionResult Index(EmpresaVM model)
        {

            if (ModelState.IsValid)
            {
                if (model.Id == 0)
                {
                    _context.Add(model.Insert(model));
                    _context.SaveChanges();
                    _notify.Success("Empresa cadastrada com sucesso.");
                }
                else
                {
                    _context.Update(model.Update(_context, model.Id));
                    _context.SaveChanges();
                    _notify.Success("Empresa editada com sucesso.");
                }
            }
            else
            {
                _notify.Error();
            }

            return RedirectToAction(nameof(Index));
        }

        public IActionResult AjaxEmpresa(int id = 0)
        {
            if (id == 0)
                return View(new EmpresaVM());
            else
                return View(new EmpresaVM(_context, id));

        }

        public IActionResult Delete(int id)
        {
            var empresa = _context.Empresas.FirstOrDefault(c => c.Id == id);
            empresa.IsAtivo = false;
            _context.Update(empresa);
            _context.SaveChanges();
            _notify.Success("Empresa apagada com sucesso.");

            return RedirectToAction(nameof(Index));
        }
    }
}