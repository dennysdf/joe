﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JOE.Areas.Admin.Models.Alunos;
using JOE.Models.DBJOE.Context;
using JOE.Models.DBJOE.Tables;
using JOE.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace JOE.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Policy = "Admin")]
    public class AlunosController : Controller
    {
        private readonly DBJOEContext _context;
        private readonly IUser _user;
        public readonly INotificacao _notify;

        public AlunosController(DBJOEContext context, IUser user, INotificacao notify)
        {
            _context = context;
            _user = user;
            _notify = notify;
        }

        public IActionResult Index()
        {
            var alunos = _context.Usuarios.Include(c => c.UserCurso).ThenInclude(c => c.Curso).Where(c => c.TipoId == 3 && c.IsAtivo).Select(c => new AlunoVM
            {
                Id = c.Id,
                Nome = c.Nome,
                Curso = c.UserCurso.Select(x => x.Curso.Descricao).First(),
                Email = c.Email,
                Telefone = c.Telefone,
                Matricula = c.Matricula,
                Cidade = c.Cidade,
                Endereco = c.Endereco
            }).ToList();

            return View(alunos);
        }

        [HttpPost]
        public IActionResult Index(AlunoVM model )
        {
            if (ModelState.IsValid)
            {
                if (model.Id == 0)
                {
                    var aluno = model.Insert(model, _user.Instituicao);
                    _context.Add(aluno);
                    _context.SaveChanges();
                    var cursoAluno = new UsuarioCurso() { CursoId = model.CursoId, UsuarioId = aluno.Id };
                    _context.Add(cursoAluno);
                    _context.SaveChanges();
                    _notify.Success("Aluno cadastrado com sucesso.");
                }
                else
                {   _context.Update(model.Update( model.Id, _context));
                    _context.SaveChanges();
                    _notify.Success("Aluno editado com sucesso.");
                }
            }
            else
            {
                _notify.Error();
            }

            return RedirectToAction(nameof(Index));
        }

        public IActionResult AjaxAluno(int id = 0)
        {
            if(id == 0)
                return View(new AlunoVM(_context));
            else                
                return View(new AlunoVM(_context, id));
        }

        public bool Matricula(string Matricula)
        {
            return !_context.Usuarios.Any(c => c.Matricula.Equals(Matricula));
        }

        public IActionResult Delete (int id)
        {
            var aluno = _context.Usuarios.FirstOrDefault(c => c.Id == id);
            aluno.IsAtivo = false;
            _context.Update(aluno);
            _context.SaveChanges();
            _notify.Success("Aluno apagado com sucesso.");

            return RedirectToAction(nameof(Index));
        }
    }
}