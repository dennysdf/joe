﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JOE.Areas.Admin.Models.Professores;
using JOE.Models.DBJOE.Context;
using JOE.Models.DBJOE.Tables;
using JOE.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace JOE.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Policy = "Admin")]
    public class ProfessoresController : Controller
    {
        private readonly DBJOEContext _context;
        private readonly IUser _user;
        private readonly INotificacao _notify;

        public ProfessoresController(DBJOEContext context, IUser user, INotificacao notify)
        {
            _context = context;
            _user = user;
            _notify = notify;
        }

        public IActionResult Index()
        {
            var professores = _context.Usuarios.Include(c => c.UserCurso).ThenInclude(c => c.Curso).Where(c => c.TipoId == 2 && c.IsAtivo).Select(c => new ProfessorVM
            {
                Id = c.Id,
                Nome = c.Nome,
                Curso = c.UserCurso.Select(x => x.Curso.Descricao).ToList(),
                Email = c.Email,
                Telefone = c.Telefone,
                Matricula = c.Matricula,
                Cidade = c.Cidade,
                Endereco = c.Endereco
            }).ToList();

            return View(professores);
        }

        [HttpPost]
        public IActionResult Index(ProfessorVM model)
        {
            if (ModelState.IsValid)
            {
                if (model.Id == 0)
                {
                    var aluno = model.Insert(model, _user.Instituicao);
                    _context.Add(aluno);
                    _context.SaveChanges();
                    var cursoProfessor = model.CursoId.Select( (v, i) => new UsuarioCurso { UsuarioId = aluno.Id, CursoId = v  }).ToList();         
                    _context.AddRange(cursoProfessor);
                    _context.SaveChanges();
                    _notify.Success("Professor cadastrado com sucesso.");
                }
                else
                {
                    _context.Update(model.Update(model.Id, _context));
                    _context.RemoveRange(_context.UsuariosCurso.Where(c => c.UsuarioId == model.Id).ToList());
                    var cursoProfessor = model.CursoId.Select((v, i) => new UsuarioCurso { UsuarioId = model.Id, CursoId = v }).ToList();
                    _context.AddRange(cursoProfessor);
                    _context.SaveChanges();
                    _notify.Success("Professor editado com sucesso.");
                }
            }
            else
            {
                _notify.Error();
            }

            return RedirectToAction(nameof(Index));
        }

        public IActionResult AjaxProfessor(int id = 0)
        {
            if (id == 0)
                return View(new ProfessorVM(_context));
            else
                return View(new ProfessorVM(_context, id));
        }

        public IActionResult Delete(int id)
        {
            var empresa = _context.Usuarios.FirstOrDefault(c => c.Id == id);
            empresa.IsAtivo = false;
            _context.Update(empresa);
            _context.SaveChanges();
            _notify.Success("Professor apagado com sucesso.");

            return RedirectToAction(nameof(Index));
        }
    }
}