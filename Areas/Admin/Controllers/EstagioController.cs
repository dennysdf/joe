﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JOE.Areas.Admin.Models.Estagio;
using JOE.Models.DBJOE.Context;
using JOE.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace JOE.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Policy = "Admin")]
    public class EstagioController : Controller
    {
        private readonly DBJOEContext _context;
        private readonly IUser _user;
        private readonly INotificacao _notify;

        public EstagioController(DBJOEContext context, IUser user, INotificacao notify)
        {
            _context = context;
            _user = user;
            _notify = notify;
        }

        public IActionResult Index()
        {
            var estagio = _context.OrientadoresEstagiarios
                        .Include(c => c.Empresa)
                        .Include(c => c.Orientador)
                        .Include(c => c.Estagiario)
                        .Where(c => c.IsAtivo && !c.IsFinalizado)
                        .Select(c => new EstagioVM { Id = c.Id, Estagiario = c.Estagiario.Nome, Orientador = c.Orientador.Nome, EmpresaName = c.Empresa.Nome, PrevisaoEstagio = c.Previsao.ToShortDateString(), Relatorio = c.Previsao.AddDays(90) })
                        .ToList();

            return View(estagio);
        }

        [HttpPost]
        public IActionResult Index(EstagioVM model)
        {
            if (ModelState.IsValid)
            {
                if (model.Id == 0)
                {
                    _context.Add(model.Insert(model));
                    _context.SaveChanges();
                    _notify.Success("Estágio cadastrado com sucesso.");
                }
                else
                {
                    _context.Update(model.Update(_context, model.Id));
                    _context.SaveChanges();
                    _notify.Success("Estágio editado com sucesso.");
                }
            }
            else
            {
                _notify.Error();
            }

            return RedirectToAction(nameof(Index));
        }

        public string JsonOrientadores(int id)
        {
            var curso = _context.UsuariosCurso.First(c => c.UsuarioId == id).CursoId;
            var professores = _context.Usuarios.Include(c => c.UserCurso).Where(c => c.IsAtivo && c.TipoId == 2 && c.UserCurso.Any(x => x.CursoId == curso)).Select(c => new { c.Id, c.Nome }).ToList();

            return JsonConvert.SerializeObject(professores);
        }

        public string JsonEmpresas(int id)
        {
            var empresa = _context.Empresas.Where(c => c.IsAtivo).Select(c => new { c.Id, c.Nome }).ToList();
            return JsonConvert.SerializeObject(empresa);
        }
        
        public IActionResult AjaxEstagio(int id = 0)
        {

            if (id == 0)
                return View(new EstagioVM(_context));
            else
                return View(new EstagioVM(_context, id));
        }

        public IActionResult Delete(int id)
        {
            var empresa = _context.OrientadoresEstagiarios.FirstOrDefault(c => c.Id == id);
            empresa.IsAtivo = false;
            _context.Update(empresa);
            _context.SaveChanges();
            _notify.Success("Estágio apagado com sucesso.");

            return RedirectToAction(nameof(Index));
        }

        public IActionResult Finalizar(int id)
        {
            var empresa = _context.OrientadoresEstagiarios.FirstOrDefault(c => c.Id == id);
            empresa.IsFinalizado = true;
            _context.Update(empresa);
            _context.SaveChanges();
            _notify.Success("Estágio finalizado com sucesso.");

            return RedirectToAction(nameof(Index));
        }

    }
}