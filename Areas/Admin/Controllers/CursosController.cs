﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JOE.Areas.Admin.Models.Cursos;
using JOE.Models.DBJOE.Context;
using JOE.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace JOE.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Policy = "Admin")]
    public class CursosController : Controller
    {
        private readonly DBJOEContext _context;
        private readonly IUser _user;
        public readonly INotificacao _notify;

        public CursosController(DBJOEContext context, IUser user, INotificacao notify)
        {
            _context = context;
            _user = user;
            _notify = notify;
        }

        public IActionResult Index()
        {
            var cursos = _context.Cursos.Include(c => c.Instituicao).Where(c => c.IsAtivo).Select(c => new CursosVM { Id = c.Id, Descricao = c.Descricao, Instituicao = c.Instituicao.Descricao }).ToList();

            return View(cursos);
        }

        [HttpPost]
        public IActionResult Index(CursosVM model)
        {
            if (ModelState.IsValid)
            {
                if (model.Id == 0)
                {
                    _context.Add(model.Insert(model, _user.Instituicao));
                    _context.SaveChanges();
                    _notify.Success("Curso cadastrado com sucesso.");
                }
                else
                {
                    _context.Update(model.Update(_context, model.Id));
                    _context.SaveChanges();
                    _notify.Success("Curso editado com sucesso.");
                }
            }
            else
            {
                _notify.Error();
            }

            return RedirectToAction(nameof(Index));
        }

        public IActionResult AjaxCurso(int id = 0)
        {
            if (id == 0)
                return View(new CursosVM());
            else
                return View(new CursosVM(_context, id));
        }

        public IActionResult Delete(int id)
        {
            var curso = _context.Cursos.FirstOrDefault(c => c.Id == id);
            curso.IsAtivo = false;
            _context.Update(curso);
            _context.SaveChanges();
            _notify.Success("Curso apagado com sucesso.");

            return RedirectToAction(nameof(Index));
        }
    }
}