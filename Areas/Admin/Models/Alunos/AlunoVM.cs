﻿using JOE.Models.DBJOE.Context;
using JOE.Models.DBJOE.Mappers;
using JOE.Models.DBJOE.Tables;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JOE.Areas.Admin.Models.Alunos
{
    public class AlunoVM
    {
        
        public int Id { get; set; }
        [Required(ErrorMessage = Global.Required)]
        [MaxLength(100, ErrorMessage = Global.MaxLength)]
        public string Nome { get; set; }
        [Required(ErrorMessage = Global.Required)]
        [MaxLength(14, ErrorMessage = Global.MaxLength)]
        [Remote("Matricula","Alunos",ErrorMessage = Global.Matricula)]
        public string Matricula { get; set; }
        [Required(ErrorMessage = Global.Required)]
        [MaxLength(50, ErrorMessage = Global.MaxLength)]
        public string Cidade { get; set; }
        [Required(ErrorMessage = Global.Required)]
        [MaxLength(100, ErrorMessage = Global.MaxLength)]
        public string Endereco { get; set; }
        [Required(ErrorMessage = Global.Required)]
        public string Telefone { get; set; }
        [Required(ErrorMessage = Global.Required)]
        [MaxLength(100, ErrorMessage = Global.MaxLength)]
        [DataType(DataType.EmailAddress, ErrorMessage = Global.Email,ErrorMessageResourceName = Global.Email)]
        public string Email { get; set; }        
        [Required(ErrorMessage = Global.Required)]
        public int CursoId { get; set; }
        public string Curso { get; set; }

        public SelectList Cursos { get; set; }

        public AlunoVM()
        {

        }

        public AlunoVM(DBJOEContext _context)
        {
            var cursos = _context.Cursos.Where(c => c.IsAtivo).Select(c => new { c.Id, c.Descricao }).ToList();
            Cursos = new SelectList(cursos, "Id", "Descricao");
        }

        public AlunoVM(DBJOEContext _context, int id)
        {
            var cursos = _context.Cursos.Where(c => c.IsAtivo).Select(c => new { c.Id, c.Descricao }).ToList();
            Cursos = new SelectList(cursos, "Id", "Descricao");
            var user = _context.Usuarios.Include(C => C.UserCurso).ThenInclude(c => c.Curso).First(c => c.Id == id);
            Id = user.Id;
            Nome = user.Nome;
            Matricula = user.Matricula;
            Cidade = user.Cidade;
            Endereco = user.Endereco;
            Telefone = user.Telefone;
            Email = user.Email;            
            CursoId = _context.UsuariosCurso.FirstOrDefault(c => c.UsuarioId == id).CursoId;
            Curso =  user.UserCurso.Select(c => c.Curso.Descricao).First();
        }

        public Usuario Insert(AlunoVM model, int idInstituicao)
        {
            return new Usuario()
            {
                Nome = this.Nome,
                Matricula = this.Matricula,
                Cidade = this.Cidade,
                Endereco = this.Endereco,
                Email = this.Email,
                InstituicaoId = idInstituicao,
                Senha = "123456",
                Telefone = this.Telefone,
                TipoId = 3                
            };
        }

        public Usuario Update(int id, DBJOEContext _context)
        {
            var aluno = _context.Usuarios.Include(c => c.UserCurso).FirstOrDefault(c => c.Id == id);
            aluno.Nome = this.Nome;
            aluno.Cidade = this.Cidade;
            aluno.Endereco = this.Endereco;
            aluno.Email = this.Email;
            aluno.Telefone = this.Telefone;
            aluno.UserCurso.ToList().ForEach(c => c.CursoId = this.CursoId);            
            return aluno;
        }
    }
}
