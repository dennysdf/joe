﻿using JOE.Models.DBJOE.Context;
using JOE.Models.DBJOE.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JOE.Areas.Admin.Models.Empresas
{
    public class EmpresaVM
    {
        public int Id { get; set; }
        [Required(ErrorMessage = Global.Required)]
        public string Nome { get; set; }
        [Required(ErrorMessage = Global.Required)]
        public string RazaoSocial { get; set; }
        [Required(ErrorMessage = Global.Required)]
        public string CNPJ { get; set; }
        [Required(ErrorMessage = Global.Required)]
        public string Endereco { get; set; }
        [Required(ErrorMessage = Global.Required)]
        public string Telefone { get; set; }
        public string Site { get; set; }
        [Required(ErrorMessage = Global.Required)]
        public string AreaAtuacao { get; set; }
        [Required(ErrorMessage = Global.Required)]
        public string Servico { get; set; }

        public EmpresaVM()
        {

        }

        public EmpresaVM(DBJOEContext _context, int id)
        {
            var empresa = _context.Empresas.FirstOrDefault(c => c.Id == id);
            Nome = empresa.Nome;
            AreaAtuacao = empresa.AreaAtuacao;
            CNPJ = empresa.CNPJ;
            Endereco = empresa.Endereco;
            RazaoSocial = empresa.RazaoSocial;
            Servico = empresa.Servico;
            Site = empresa.Site;
            Telefone = empresa.Telefone;
        }

        public Empresa Insert(EmpresaVM model)
        {
            return new Empresa
            {
                Nome = this.Nome,
                AreaAtuacao = this.AreaAtuacao,
                CNPJ = this.CNPJ,
                Endereco = this.Endereco,
                RazaoSocial = this.RazaoSocial,
                Servico = this.Servico,
                Site = this.Site,
                Telefone = this.Telefone
            };
        }

        public Empresa Update(DBJOEContext _context, int id)
        {
            var empresa = _context.Empresas.FirstOrDefault(c => c.Id == id);
            empresa.Nome = this.Nome;
            empresa.AreaAtuacao = this.AreaAtuacao;
            empresa.CNPJ = this.CNPJ;
            empresa.Endereco = this.Endereco;
            empresa.RazaoSocial = this.RazaoSocial;
            empresa.Servico = this.Servico;
            empresa.Site = this.Site;
            empresa.Telefone = this.Telefone;
            return empresa;
        }
    }
}
