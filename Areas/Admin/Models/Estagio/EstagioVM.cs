﻿using JOE.Models.DBJOE.Context;
using JOE.Models.DBJOE.Tables;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JOE.Areas.Admin.Models.Estagio
{
    public class EstagioVM
    {
        public int Id { get; set; }

        [Required(ErrorMessage = Global.Required)]
        public int AlunoId { get; set; }
        public SelectList Alunos { get; set; }

        [Required(ErrorMessage = Global.Required)]
        public int OrientadorId { get; set; }
        public SelectList Orientadores { get; set; }

        [Required(ErrorMessage = Global.Required)]
        public int EmpresaId { get; set; }
        public SelectList Empresa { get; set; }
        public string EmpresaName { get; set; }

        [Required(ErrorMessage = Global.Required)]
        public string Previsao { get; set; }
        [Required(ErrorMessage = Global.Required)]
        public string Supervisor { get; set; }
        public DateTime Relatorio { get; internal set; }
        public string Estagiario { get; internal set; }
        public string PrevisaoEstagio { get; internal set; }
        public string Orientador { get; internal set; }

        public EstagioVM()
        {

        }

        public EstagioVM(DBJOEContext _context)
        {
            var estagiarios = _context.OrientadoresEstagiarios.Include(c => c.Estagiario).Where(c => c.IsAtivo && c.Estagiario.IsAtivo && !c.IsFinalizado) .Select(c => c.EstagiarioId);
            var alunos = _context.Usuarios.Where(c => c.TipoId == 3 && !estagiarios.Contains(c.Id) && c.IsAtivo ).Select(c => new { c.Id, c.Nome }).ToList();
            Alunos = new SelectList(alunos, "Id", "Nome");
        }

        public EstagioVM(DBJOEContext _context, int id)
        {
            var estagio = _context.OrientadoresEstagiarios.FirstOrDefault(c => c.Id == id);
            Id = estagio.Id;
            var estagiarios = _context.OrientadoresEstagiarios.Select(c => c.EstagiarioId);

            var alunos = _context.Usuarios.Where(c => (c.TipoId == 3 && c.IsAtivo && !estagiarios.Contains(c.Id)) || (c.Id == estagio.EstagiarioId)).Select(c => new { c.Id, c.Nome }).ToList();
            Alunos = new SelectList(alunos, "Id", "Nome");
            AlunoId = estagio.EstagiarioId;

            var curso = _context.UsuariosCurso.First(c => c.UsuarioId == estagio.EstagiarioId).CursoId;            
            var professores = _context.Usuarios.Include(c => c.UserCurso).Where(c => c.TipoId == 2 && c.UserCurso.Any(x => x.CursoId == curso)).Select(c => new { c.Id, c.Nome }).ToList();
            OrientadorId = estagio.OrientadorId;
            Orientadores = new SelectList(professores, "Id", "Nome");

            var empresas = _context.Empresas.Where(c => c.IsAtivo).Select(c => new { c.Id, c.Nome }).ToList();
            Empresa = new SelectList(empresas,"Id", "Nome");
            EmpresaId = estagio.EmpresaId;
            Supervisor = estagio.Supervisor;
            Previsao = estagio.Previsao.ToShortDateString();
        }

        public OrientadorEstagiario Insert(EstagioVM model)
        {
            return new OrientadorEstagiario
            {
                EmpresaId = model.EmpresaId,
                OrientadorId = model.OrientadorId,
                EstagiarioId = model.AlunoId,
                Supervisor = model.Supervisor,
                Previsao = DateTime.Parse(model.Previsao)
            };
        }

        public OrientadorEstagiario Update(DBJOEContext _context, int id)
        {
            var estagio = _context.OrientadoresEstagiarios.First(c => c.Id == id);
            estagio.OrientadorId = this.OrientadorId;
            estagio.EmpresaId = this.EmpresaId;
            estagio.EstagiarioId = this.AlunoId;
            estagio.Previsao = DateTime.Parse(Previsao);
            estagio.Supervisor = Supervisor;
            return estagio;
        }

    }
}
