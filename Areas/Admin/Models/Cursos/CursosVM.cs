﻿using JOE.Models.DBJOE.Context;
using JOE.Models.DBJOE.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JOE.Areas.Admin.Models.Cursos
{
    public class CursosVM
    {
        public int Id { get; set; }
        [Required(ErrorMessage = Global.Required)]
        [MaxLength(80, ErrorMessage = Global.MaxLength)]
        public string Descricao { get; set; }
        public string Instituicao { get; internal set; }

        public CursosVM()
        {

        }          

        public CursosVM(DBJOEContext _context, int id)
        {
            var curso = _context.Cursos.FirstOrDefault(c => c.Id == id);
            this.Descricao = curso.Descricao;
            this.Id = curso.Id;
        }

        public Curso Insert(CursosVM model, int id)
        {
            return new Curso
            {
                Descricao = this.Descricao,
                InstituicaoId = id                
            };
        }

        public Curso Update(DBJOEContext _context, int id)
        {
            var curso = _context.Cursos.FirstOrDefault(c => c.Id == id);
            curso.Descricao = this.Descricao;
            return curso;

        }
    }
}
