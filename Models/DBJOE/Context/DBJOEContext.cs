﻿using JOE.Models.DBJOE.Mappers;
using JOE.Models.DBJOE.Tables;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JOE.Models.DBJOE.Context
{
    public class DBJOEContext : DbContext
    {
        public DBJOEContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<TipoUser> TiposUser { get; set; }
        public DbSet<Instituicao> Instituicoes { get; set; }
        public DbSet<Curso> Cursos { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<UsuarioCurso> UsuariosCurso { get; set; }
        public DbSet<Empresa> Empresas { get; set; }
        public DbSet<OrientadorEstagiario> OrientadoresEstagiarios { get; set; }
        public DbSet<Relatorio> Relatorios { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new TipoUserEC());
            modelBuilder.ApplyConfiguration(new InstituicaoEC());
            modelBuilder.ApplyConfiguration(new CursoEC());
            modelBuilder.ApplyConfiguration(new UsuarioEC());
            modelBuilder.ApplyConfiguration(new UsuarioCursoEC());
            modelBuilder.ApplyConfiguration(new EmpresaEC()); 
            modelBuilder.ApplyConfiguration(new OrientadorEstagiarioEC());
            modelBuilder.ApplyConfiguration(new RelatorioEC());
        }
    }
}
