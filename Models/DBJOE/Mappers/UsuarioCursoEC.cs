﻿using JOE.Models.DBJOE.Tables;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JOE.Models.DBJOE.Mappers
{
    public class UsuarioCursoEC : IEntityTypeConfiguration<UsuarioCurso>
    {
        public void Configure(EntityTypeBuilder<UsuarioCurso> builder)
        {
            builder.HasOne(c => c.Curso)
                .WithMany(c => c.UserCurso)
                .HasForeignKey(c => c.CursoId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasOne(c => c.Usuario)
                .WithMany(c => c.UserCurso)
                .HasForeignKey(c => c.UsuarioId)
                .OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}
