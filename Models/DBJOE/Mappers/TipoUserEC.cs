﻿using JOE.Models.DBJOE.Tables;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JOE.Models.DBJOE.Mappers
{
    public class TipoUserEC : IEntityTypeConfiguration<TipoUser>
    {
        public void Configure(EntityTypeBuilder<TipoUser> builder)
        {
            builder.Property(c => c.IsAtivo)
                .HasDefaultValue(true);
        }
    }
}
