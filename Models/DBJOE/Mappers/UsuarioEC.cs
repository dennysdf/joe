﻿using JOE.Models.DBJOE.Tables;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JOE.Models.DBJOE.Mappers
{
    public class UsuarioEC : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.HasOne(c => c.Tipo)
                .WithMany(c => c.Usuarios)
                .HasForeignKey(c => c.TipoId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasOne(c => c.Instituicao)
                .WithMany(c => c.Usuarios)
                .HasForeignKey(c => c.InstituicaoId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.Property(c => c.DateAdd)
                .HasDefaultValueSql("(getdate())");

            builder.Property(c => c.IsAtivo)
                .HasDefaultValue(true);

        }
    }
}
