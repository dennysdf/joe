﻿using JOE.Models.DBJOE.Tables;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JOE.Models.DBJOE.Mappers
{
    public class RelatorioEC : IEntityTypeConfiguration<Relatorio>
    {
        public void Configure(EntityTypeBuilder<Relatorio> builder)
        {
            builder.HasOne(c => c.Estagio)
                .WithMany(c => c.Relatorios)
                .HasForeignKey(c => c.EstagioId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.Property(c => c.DateAdd)
                .HasDefaultValueSql("(getdate())");
        }
    }
}
