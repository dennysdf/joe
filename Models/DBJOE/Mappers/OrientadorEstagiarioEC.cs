﻿using JOE.Models.DBJOE.Tables;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JOE.Models.DBJOE.Mappers
{
    public class OrientadorEstagiarioEC : IEntityTypeConfiguration<OrientadorEstagiario>
    {
        public void Configure(EntityTypeBuilder<OrientadorEstagiario> builder)
        {
            builder.HasOne(c => c.Empresa)
                .WithMany(c => c.OrientadoresEstagiario)
                .HasForeignKey(c => c.EmpresaId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasOne(c => c.Estagiario)
                .WithMany(c => c.Estagiarios)
                .HasForeignKey(c => c.EstagiarioId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasOne(c => c.Orientador)
                .WithMany(c => c.Orientadores)
                .HasForeignKey(c => c.OrientadorId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.Property(c => c.IsAtivo)
                .HasDefaultValue(true);

            builder.Property(c => c.IsFinalizado)
                .HasDefaultValue(false);

        }
    }
}

