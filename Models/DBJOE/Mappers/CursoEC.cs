﻿using JOE.Models.DBJOE.Tables;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JOE.Models.DBJOE.Mappers
{
    public class CursoEC : IEntityTypeConfiguration<Curso>
    {
        public void Configure(EntityTypeBuilder<Curso> builder)
        {
            builder.HasOne(c => c.Instituicao)
                .WithMany(c => c.Cursos)
                .HasForeignKey(c => c.InstituicaoId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.Property(c => c.IsAtivo)
                .HasDefaultValue(true);
        }
    }
}
