﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JOE.Models.DBJOE.Tables
{
    public class Curso
    {
        public Curso()
        {
            UserCurso = new HashSet<UsuarioCurso>();
        }

        public int Id { get; set; }
        public string Descricao { get; set; }
        public int InstituicaoId { get; set; }
        public Instituicao Instituicao { get; set; }
        public bool IsAtivo { get; set; }
        public ICollection<UsuarioCurso> UserCurso { get; set; }
    }
}
