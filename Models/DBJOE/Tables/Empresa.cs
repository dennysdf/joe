﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JOE.Models.DBJOE.Tables
{
    public class Empresa
    {
        public Empresa()
        {
            OrientadoresEstagiario = new HashSet<OrientadorEstagiario>();
        }

        public int Id { get; set; }
        public string Nome { get; set; }
        public string RazaoSocial { get; set; }
        public string CNPJ { get; set; }
        public string Endereco { get; set; }
        public string Telefone { get; set; }
        public string Site { get; set; }
        public string AreaAtuacao { get; set; }
        public string Servico { get; set; }
        public bool IsAtivo { get; set; }
        public ICollection<OrientadorEstagiario> OrientadoresEstagiario { get; set; }
    }
}
