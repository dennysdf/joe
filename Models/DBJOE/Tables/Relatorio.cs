﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JOE.Models.DBJOE.Tables
{
    public class Relatorio
    {
        public int Id { get; set; }
        public int EstagioId { get; set; }
        public OrientadorEstagiario Estagio { get; set; }
        public int ResponsavelId { get; set; }
        public Usuario Responsavel { get; set; }
        public string Descricao { get; set; }
        public string NameFile { get; set; }
        public DateTime DateAdd { get; set; }
    }
}
