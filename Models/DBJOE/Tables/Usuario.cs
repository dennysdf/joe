﻿using JOE.Models.DBJOE.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JOE.Models.DBJOE.Tables
{
    public class Usuario
    {
        public Usuario()
        {
            UserCurso = new HashSet<UsuarioCurso>();
            Estagiarios = new HashSet<OrientadorEstagiario>();
            Orientadores = new HashSet<OrientadorEstagiario>();
        }

        public int Id { get; set; }
        public int TipoId { get; set; }
        public TipoUser Tipo { get; set; }
        public int InstituicaoId { get; set; }
        public Instituicao Instituicao { get; set; }
        public string Nome { get; set; }
        public string Matricula { get; set; }
        public string Senha { get; set; }
        public string Cidade { get; set; }
        public string Endereco { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }        
        public DateTime DateAdd { get; set; }
        public DateTime? LastAcesso { get; set; }
        public bool IsAtivo { get; set; }
        public ICollection<UsuarioCurso> UserCurso { get; set; }
        public ICollection<OrientadorEstagiario> Estagiarios { get; set; }
        public ICollection<OrientadorEstagiario> Orientadores { get; set; }
    }
}
