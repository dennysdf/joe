﻿
using JOE.Models.DBJOE.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JOE.Models.DBJOE.Tables
{
    public class UsuarioCurso
    {
        public int Id { get; set; }
        public int UsuarioId { get; set; }
        public Usuario Usuario { get; set; }
        public int CursoId { get; set; }
        public Curso Curso { get; set; }
    }
}
