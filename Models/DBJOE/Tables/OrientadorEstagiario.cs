﻿using JOE.Models.DBJOE.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JOE.Models.DBJOE.Tables
{
    public class OrientadorEstagiario
    {
        public OrientadorEstagiario()
        {
            Relatorios = new HashSet<Relatorio>();
        }

        public int Id { get; set; }
        public int EstagiarioId { get; set; }
        public Usuario Estagiario { get; set; }
        public int OrientadorId { get; set; }
        public Usuario Orientador { get; set; }
        public int EmpresaId { get; set; }
        public Empresa Empresa { get; set; }
        public string Supervisor { get; set; }
        public DateTime Previsao { get; set; }
        public bool IsAtivo { get; set; }
        public bool IsFinalizado { get; set; }
        public ICollection<Relatorio> Relatorios { get; set; }
    }
}
