﻿using JOE.Models.DBJOE.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JOE.Models.DBJOE.Tables
{
    public class Instituicao
    {
        public Instituicao()
        {
            Cursos = new HashSet<Curso>();
            Usuarios = new HashSet<Usuario>();
        }

        public int Id { get; set; }
        public string Descricao { get; set; }
        public bool IsAtivo { get; set; }
        public ICollection<Curso> Cursos { get; set; }
        public ICollection<Usuario> Usuarios { get; set; }
    }
}
