﻿// <auto-generated />
using System;
using JOE.Models.DBJOE.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace JOE.Migrations
{
    [DbContext(typeof(DBJOEContext))]
    partial class DBJOEContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("JOE.Models.DBJOE.Tables.Curso", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Descricao");

                    b.Property<int>("InstituicaoId");

                    b.Property<bool>("IsAtivo")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(true);

                    b.HasKey("Id");

                    b.HasIndex("InstituicaoId");

                    b.ToTable("Cursos");
                });

            modelBuilder.Entity("JOE.Models.DBJOE.Tables.Empresa", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("AreaAtuacao");

                    b.Property<string>("CNPJ");

                    b.Property<string>("Endereco");

                    b.Property<bool>("IsAtivo")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(true);

                    b.Property<string>("Nome");

                    b.Property<string>("RazaoSocial");

                    b.Property<string>("Servico");

                    b.Property<string>("Site");

                    b.Property<string>("Telefone");

                    b.HasKey("Id");

                    b.ToTable("Empresas");
                });

            modelBuilder.Entity("JOE.Models.DBJOE.Tables.Instituicao", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Descricao");

                    b.Property<bool>("IsAtivo")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(true);

                    b.HasKey("Id");

                    b.ToTable("Instituicoes");
                });

            modelBuilder.Entity("JOE.Models.DBJOE.Tables.OrientadorEstagiario", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("EmpresaId");

                    b.Property<int>("EstagiarioId");

                    b.Property<bool>("IsAtivo")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(true);

                    b.Property<bool>("IsFinalizado")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(false);

                    b.Property<int>("OrientadorId");

                    b.Property<DateTime>("Previsao");

                    b.Property<string>("Supervisor");

                    b.HasKey("Id");

                    b.HasIndex("EmpresaId");

                    b.HasIndex("EstagiarioId");

                    b.HasIndex("OrientadorId");

                    b.ToTable("OrientadoresEstagiarios");
                });

            modelBuilder.Entity("JOE.Models.DBJOE.Tables.Relatorio", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("DateAdd")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("(getdate())");

                    b.Property<string>("Descricao");

                    b.Property<int>("EstagioId");

                    b.Property<string>("NameFile");

                    b.Property<int>("ResponsavelId");

                    b.HasKey("Id");

                    b.HasIndex("EstagioId");

                    b.HasIndex("ResponsavelId");

                    b.ToTable("Relatorios");
                });

            modelBuilder.Entity("JOE.Models.DBJOE.Tables.TipoUser", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Descricao");

                    b.Property<bool>("IsAtivo")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(true);

                    b.HasKey("Id");

                    b.ToTable("TiposUser");
                });

            modelBuilder.Entity("JOE.Models.DBJOE.Tables.Usuario", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Cidade");

                    b.Property<DateTime>("DateAdd")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("(getdate())");

                    b.Property<string>("Email");

                    b.Property<string>("Endereco");

                    b.Property<int>("InstituicaoId");

                    b.Property<bool>("IsAtivo")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(true);

                    b.Property<DateTime?>("LastAcesso");

                    b.Property<string>("Matricula");

                    b.Property<string>("Nome");

                    b.Property<string>("Senha");

                    b.Property<string>("Telefone");

                    b.Property<int>("TipoId");

                    b.HasKey("Id");

                    b.HasIndex("InstituicaoId");

                    b.HasIndex("TipoId");

                    b.ToTable("Usuarios");
                });

            modelBuilder.Entity("JOE.Models.DBJOE.Tables.UsuarioCurso", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("CursoId");

                    b.Property<int>("UsuarioId");

                    b.HasKey("Id");

                    b.HasIndex("CursoId");

                    b.HasIndex("UsuarioId");

                    b.ToTable("UsuariosCurso");
                });

            modelBuilder.Entity("JOE.Models.DBJOE.Tables.Curso", b =>
                {
                    b.HasOne("JOE.Models.DBJOE.Tables.Instituicao", "Instituicao")
                        .WithMany("Cursos")
                        .HasForeignKey("InstituicaoId");
                });

            modelBuilder.Entity("JOE.Models.DBJOE.Tables.OrientadorEstagiario", b =>
                {
                    b.HasOne("JOE.Models.DBJOE.Tables.Empresa", "Empresa")
                        .WithMany("OrientadoresEstagiario")
                        .HasForeignKey("EmpresaId");

                    b.HasOne("JOE.Models.DBJOE.Tables.Usuario", "Estagiario")
                        .WithMany("Estagiarios")
                        .HasForeignKey("EstagiarioId");

                    b.HasOne("JOE.Models.DBJOE.Tables.Usuario", "Orientador")
                        .WithMany("Orientadores")
                        .HasForeignKey("OrientadorId");
                });

            modelBuilder.Entity("JOE.Models.DBJOE.Tables.Relatorio", b =>
                {
                    b.HasOne("JOE.Models.DBJOE.Tables.OrientadorEstagiario", "Estagio")
                        .WithMany("Relatorios")
                        .HasForeignKey("EstagioId");

                    b.HasOne("JOE.Models.DBJOE.Tables.Usuario", "Responsavel")
                        .WithMany()
                        .HasForeignKey("ResponsavelId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("JOE.Models.DBJOE.Tables.Usuario", b =>
                {
                    b.HasOne("JOE.Models.DBJOE.Tables.Instituicao", "Instituicao")
                        .WithMany("Usuarios")
                        .HasForeignKey("InstituicaoId");

                    b.HasOne("JOE.Models.DBJOE.Tables.TipoUser", "Tipo")
                        .WithMany("Usuarios")
                        .HasForeignKey("TipoId");
                });

            modelBuilder.Entity("JOE.Models.DBJOE.Tables.UsuarioCurso", b =>
                {
                    b.HasOne("JOE.Models.DBJOE.Tables.Curso", "Curso")
                        .WithMany("UserCurso")
                        .HasForeignKey("CursoId");

                    b.HasOne("JOE.Models.DBJOE.Tables.Usuario", "Usuario")
                        .WithMany("UserCurso")
                        .HasForeignKey("UsuarioId");
                });
#pragma warning restore 612, 618
        }
    }
}
