﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JOE.Migrations
{
    public partial class IsAtivo2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsAtivo",
                table: "OrientadoresEstagiarios",
                nullable: false,
                defaultValue: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsAtivo",
                table: "OrientadoresEstagiarios");
        }
    }
}
