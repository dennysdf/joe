﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JOE.Migrations
{
    public partial class AlterTableRelatorio : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Versao",
                table: "Relatorios",
                newName: "ResponsavelId");

            migrationBuilder.AddColumn<string>(
                name: "Descricao",
                table: "Relatorios",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NameFile",
                table: "Relatorios",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Relatorios_ResponsavelId",
                table: "Relatorios",
                column: "ResponsavelId");

            migrationBuilder.AddForeignKey(
                name: "FK_Relatorios_Usuarios_ResponsavelId",
                table: "Relatorios",
                column: "ResponsavelId",
                principalTable: "Usuarios",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Relatorios_Usuarios_ResponsavelId",
                table: "Relatorios");

            migrationBuilder.DropIndex(
                name: "IX_Relatorios_ResponsavelId",
                table: "Relatorios");

            migrationBuilder.DropColumn(
                name: "Descricao",
                table: "Relatorios");

            migrationBuilder.DropColumn(
                name: "NameFile",
                table: "Relatorios");

            migrationBuilder.RenameColumn(
                name: "ResponsavelId",
                table: "Relatorios",
                newName: "Versao");
        }
    }
}
