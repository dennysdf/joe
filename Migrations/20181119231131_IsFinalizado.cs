﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JOE.Migrations
{
    public partial class IsFinalizado : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsFinalizado",
                table: "OrientadoresEstagiarios",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsFinalizado",
                table: "OrientadoresEstagiarios");
        }
    }
}
