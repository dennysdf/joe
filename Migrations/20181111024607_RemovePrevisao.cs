﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JOE.Migrations
{
    public partial class RemovePrevisao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Previsao",
                table: "Usuarios");

            migrationBuilder.AddColumn<DateTime>(
                name: "Previsao",
                table: "OrientadoresEstagiarios",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Previsao",
                table: "OrientadoresEstagiarios");

            migrationBuilder.AddColumn<DateTime>(
                name: "Previsao",
                table: "Usuarios",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
