﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using JOE.Models;
using JOE.Models.DBJOE.Context;
using JOE.Models.Login;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;

namespace JOE.Controllers
{
    public class HomeController : Controller
    {
        public readonly DBJOEContext _context;

        public HomeController(DBJOEContext context)
        {
            _context = context;
        }

        public IActionResult Index() => View();

        [HttpPost]
        public async Task<IActionResult> Index(LoginVM model)
        {            
            if (ModelState.IsValid)
            {                
                var user = AuthenticateUser(model);                
                if (user == null)
                {
                    TempData["Erro"] = "Error";
                    return RedirectToAction("Index");
                }  
                
                var claims = new List<Claim>
                {
                    new Claim("Id", user.Id.ToString()),
                    new Claim(ClaimTypes.Name, user.Nome),
                    new Claim(ClaimTypes.Role, user.Tipo.ToString()),
                    new Claim("Email", String.IsNullOrEmpty(user.Email) ? "" : user.Email),
                    new Claim("Matricula", user.Matricula),                                        
                    new Claim("InstituicaoId", user.Instituicao.ToString())
                };

                var claimsIdentity = new ClaimsIdentity(
                    claims, CookieAuthenticationDefaults.AuthenticationScheme);
                
                var authProperties = new AuthenticationProperties
                {                    
                    ExpiresUtc = DateTime.UtcNow.AddMinutes(120),
                    AllowRefresh = true
                };

                await HttpContext.SignInAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(claimsIdentity), authProperties);

                string[] caminho = { "Admin", "Orientador", "Alunos" };
                var logado = _context.Usuarios.FirstOrDefault(c => c.Id == user.Id);
                logado.LastAcesso = DateTime.Now;
                _context.Update(logado);
                _context.SaveChanges();
                return RedirectToAction("Index","Home", new { area = caminho[user.Tipo-1]});
            }
            return RedirectToAction("Index");
        }

        private AuthenticateUser AuthenticateUser(LoginVM model)
        {
            var user =  _context.Usuarios.Where(m => Int32.Parse(m.Matricula) == model.Matricula && String.Compare(m.Senha, model.Senha, false).Equals(0)).FirstOrDefault();
            
            return user != null ? new AuthenticateUser()
            {
                Id = user.Id,
                Nome = user.Nome,
                Email = user.Email,
                Tipo = user.TipoId,
                Matricula = user.Matricula,
                Instituicao = user.InstituicaoId
            } : null;
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index", "Home");
        }

    }
}
