﻿using JOE.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using System;   
using System.Security.Claims;


namespace JOE.Services.Implementation
{
    public class User : IUser
    {
        private readonly IHttpContextAccessor _accessor;

        public User(IHttpContextAccessor accessor )
        {
            _accessor = accessor;
            Id = Int32.Parse(_accessor.HttpContext.User.FindFirstValue("Id"));
            Nome = _accessor.HttpContext.User.Identity.Name;
            Email = _accessor.HttpContext.User.FindFirstValue("Email");
            Matricula = Int32.Parse(_accessor.HttpContext.User.FindFirstValue("Matricula"));
            Instituicao = Int32.Parse(_accessor.HttpContext.User.FindFirstValue("InstituicaoId"));
        }

        public int Id { get;  }
        public string Nome { get;  }
        public string Email { get;  }
        public int Matricula { get;  }
        public int Instituicao { get;  }
    }
}
