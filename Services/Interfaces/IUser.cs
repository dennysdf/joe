﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JOE.Services.Interfaces
{
    public interface IUser
    {
        int Id { get; }
        int Matricula { get; }
        string Email { get;  }
        string Nome { get; }
        int Instituicao { get; }
    }
}
